package controller;

import java.awt.BorderLayout;
import java.awt.Button;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.Box;
import javax.swing.JFrame;

import functions.Serialization;
import gui.AccountOperations;
import gui.ClientOperations;
import gui.Menu;
import gui.Observers;
import model.Account;
import model.Bank;
import model.Client;
import model.SavingAccounts;
import model.SpendingAccounts;

@SuppressWarnings("unused")
public class Main {

	public static void main(String[] args) {
		Menu m = new Menu();
		//ClientOperations co = new ClientOperations();
		//AccountOperations co = new AccountOperations();
		/*
		Serialization s = new Serialization();
		Bank b = s.deserializate();
		Observers o = new Observers();
		o.getObservers(b);
		ArrayList<Client> clients = b.getClients();
		Set<Account> acc = b.readAccounts(clients.get(0));
		Account c = acc.iterator().next();
		System.out.println(c.getMoney());
		b.addMoney(clients.get(0), c, 100);
		System.out.println(c.getMoney());
		s.serializate(b);
		
	   /*
		Serialization s = new Serialization();
		Bank b = new Bank();
		Client c = new Client(1,"Giurgi");
	    Client d = new Client(2,"Popa");
		Client e = new Client(3,"Visovan");
		Client f = new Client(4,"Ferencz");
		b.addClient(c);
		b.addClient(d);
		b.addClient(e);
		b.addClient(f);
		Account a = new SavingAccounts(2,0);
		Account a1 = new SavingAccounts(3,0);
		Account a2 = new SpendingAccounts(4,2000);
		Account a3 = new SpendingAccounts(5,7000);
		Account a4 = new SpendingAccounts(6,16000);
		b.addAccount(c, a4);
		b.addAccount(d, a3);
		b.addAccount(f, a2);
		b.addAccount(e, a4);
		ArrayList<Client> clients = b.getClients();
		Client cl = clients.get(1);
		System.out.println(clients.get(2).getName());
		Set<Account> acc = b.readAccounts(cl);
		Account cc = acc.iterator().next();
		//System.out.println(cc.getMoney());
		b.withDrawMoney(cl, cc.getId(), 100);
		System.out.println(cc.countObservers());
		s.serializate(b); 
		
		
		/*b = s.deserializate();
		ArrayList<Client> clients = b.getClients();
		for(Client cl : clients){
		 System.out.println(cl.getName() + "  " + cl.getId());
		} */
		//s.serializate(b);
	    /*
		ArrayList<Client> clients = b.getClients();
		for(Client cl : clients){
			if(cl.getId() == 14){
				Set<Account> acc = b.readAccounts(cl);
				System.out.println(acc.size());
				for(Account aux : acc){
					System.out.println("Contul " + aux.getId() + " contine " + aux.getMoney() + " de lei si este de tipul " + aux.getClass().getSimpleName());
			}
		}
		}
		
		//b.addClient(new Client(14,"Blidar"));
		/*
		
		Client c = new Client(15,"Giurgi");
	    Client d = new Client(15,"Popa");
		Client e = new Client(44,"Visovan");
		Client f = new Client(4,"Ferencz");
		Account a = new SavingAccounts(2);
		Account a1 = new SavingAccounts(3);
		Account a2 = new SpendingAccounts(4,2000);
		Account a3 = new SpendingAccounts(5,7000);
		Account a4 = new SpendingAccounts(6,16000);
		a.adaugare(1000);
		
		b.addClient(c);
		b.addClient(d);
		b.addClient(f);
		b.addClient(e);
		b.addAccount(c, a);
		b.addAccount(c, a1);
		b.addAccount(c, a2);
		b.addAccount(c, a3);
		b.addAccount(c, a4);
		b.addAccount(d, a4);
		b.addAccount(f, a4);
		b.addAccount(f, a);
		s.serializate(b);
		/*
		System.out.println(b.getBanca().get(c).size());
		System.out.println(b.getBanca().get(d).size());
		System.out.println(b.getBanca().get(f).size());
		ArrayList<Client> clients = b.getClients();
		for(Client cl : clients){
			System.out.println(cl.getName());
		}
		
		b.addMoney(c, a2, 1111);
		Set<Account> acc = b.readAccounts(c);
			*/
		} 
	}


