package functions;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import model.Account;
import model.Bank;
import model.Client;

public class InvariantMethod implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public InvariantMethod() {
		super();
	}

	
	/*
	 * @invariant
	 */
	public boolean checkBank(Bank b){
		for(Map.Entry<Client,Set<Account>> e : b.getBanca().entrySet()){
			if(e.getKey().getName() == null)
				return false;
		}
		return true;
	}
	
}
