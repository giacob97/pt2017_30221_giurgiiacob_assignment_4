package functions;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Bank;

public class Serialization {

	public Serialization() {
		
	}
	public void serializate(Bank b){
		try{
			 FileOutputStream fileOut = new FileOutputStream("C:\\Users\\User\\Desktop\\TP\\H4\\employee.ser");
			 ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(b);
	         out.close();
	         fileOut.close();
	         //System.out.printf("Serialized data has been saved");
		}catch(IOException i) {
	         i.printStackTrace();
	      }
	}
	
	public Bank deserializate(){
		Bank b = null;
		try{
			 FileInputStream fileIn = new FileInputStream("C:\\Users\\User\\Desktop\\TP\\H4\\employee.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         b = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
		}catch(IOException i) {
	         i.printStackTrace();
	    }catch(ClassNotFoundException c) {
	         System.out.println("Bank class not found");
	         c.printStackTrace();
	    }
		return b;
	}
	
	
	
}
