package gui;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import functions.Serialization;
import model.Account;
import model.Bank;
import model.Client;
public class ClientOperations extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton add = new JButton("Add\r\n");
	JButton edit = new JButton("Edit\r\n");
	JButton delete = new JButton("Delete\r\n");
	JButton view = new JButton("View\r\n");
	JPanel contentPane = new JPanel();
	JLabel id = new JLabel("Id");
	JLabel nume = new JLabel("Nume");
	JTextField f;
	JTable t;
	JTextField f1;
	JButton a = new JButton("OK");
	Serialization s = new Serialization();
	Bank b = s.deserializate();
	JButton back = new JButton("back");
	
	public ClientOperations(){
		Draw();
		
	    add.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				restore();
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						int id = Integer.parseInt(f.getText());
						String name = f1.getText();
						f.setText("");
						f1.setText("");
						b.addClient(new Client(id,name));
	                    s.serializate(b);
	                    restore();
					}
			    	
			    });
			   
			}
	    });
	    edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				restore();
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Client> clients = b.getClients();
						String str = f.getText();
						int id = Integer.valueOf(str);
						String name = f1.getText();
						f.setText("");
						f1.setText("");
						for(Client c : clients){
							if(c.getId() == id){
								c.setName(name);
							}
						}
						restore();
						s.serializate(b);
					}
			    });
			  
			}
	    });
	    delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				restore();
			    contentPane.remove(nume);
			    contentPane.remove(f1);
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Client> clients = b.getClients();
						int id = Integer.parseInt(f.getText());
						f.setText("");
						
						for(Client c : clients){
							if(c.getId() == id){
								b.removeClient(c);
							}
						}
						restore();
					    contentPane.remove(nume);
					    contentPane.remove(f1);
						s.serializate(b);
					}
			    });
			}
	    });
	    
	    view.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contentPane.removeAll();
				Draw();
				contentPane.repaint();
				b = s.deserializate();
				CreateJTable jt = new CreateJTable();
				ArrayList<Client> clients = b.getClients();
				t = jt.setClients(clients);
				JScrollPane j = new JScrollPane(t,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				j.setBounds(250, 46, 558, 369);
				contentPane.add(j);
			    ListSelectionModel model = t.getSelectionModel();
			    model.addListSelectionListener(new ListSelectionListener(){
					@Override
					public void valueChanged(ListSelectionEvent arg0) {
						int index = model.getMinSelectionIndex();
						Set<Account> acc = b.readAccounts(clients.get(index));
						try{
						JOptionPane.showMessageDialog(null,"Clientul " + clients.get(index).getName()+ " are " + String.valueOf(acc.size()) + " conturi");
						}catch(Exception e){
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
						}
			    	
			    });
			   
				Draw();
				s.serializate(b);
			}
			
	    });
	   
	   
		back .addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Menu m = new Menu();
				dispose();
			}});
	    setVisible(true);
	}
	
	public void Draw(){
		
		setTitle("Clients\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1100, 600);
		contentPane.setBackground(new Color(102,0,0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		f = new JTextField();
		f1 = new JTextField();
		add.setBounds(80,100,100,50);
		edit.setBounds(80,180,100,50);
		delete.setBounds(80,260,100,50);
		view.setBounds(80,340,100,50);
		 back.setBounds(900, 530, 70, 20);
		 contentPane.add(back);
		contentPane.add(add);
	    contentPane.add(edit);
	    contentPane.add(delete);
	    contentPane.add(view);
	    id.setBounds(400, 175, 100, 50);
		nume.setBounds(400, 225, 100, 50);
		f.setBounds(500, 191, 150, 20);
		f1.setBounds(500, 191+50, 150, 20);
		a.setBounds(400, 300, 100, 50);
	}
	public void restore(){
		contentPane.removeAll();
		Draw();
	
		contentPane.add(id);
	    contentPane.add(nume);
	    contentPane.add(f);
	    contentPane.add(f1);
	    contentPane.add(a);
	    contentPane.repaint();
	}
}
