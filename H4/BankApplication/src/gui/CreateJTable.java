package gui;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Bank;
import model.Client;

public class CreateJTable {

	public CreateJTable() {
		super();
	}

	public JTable setClients(ArrayList<Client> arr){
		JTable t = new JTable();
		String [] col = {"Id","Nume"};
		Vector<Object>	data ;
		DefaultTableModel mdl = new DefaultTableModel(col,0);
		for(Client c : arr){
			data = new Vector<Object>();
			for(int i = 0 ; i<=1;i++){
				if(i==0)
				data.addElement(c.getId());
				else 
				data.addElement(c.getName());
			}
			mdl.addRow(data);
		}
		t.setModel(mdl);
		return t;
	}
	
	public JTable setAccounts(ArrayList<Client> arr,Bank b){
		ArrayList<Account> accounts = new ArrayList<>();
		JTable t = new JTable();
		String [] names = new String[100];
		String [] col = {"Person","Id","Tip"};//,"Depozit"};
		Vector<Object>	data ;
		int k = 0 ;
		DefaultTableModel mdl = new DefaultTableModel(col,0);
		for(Client c : arr){
			Set<Account> acc = b.readAccounts(c);
			if(acc.size()!=0){
				accounts.addAll(acc);
			for(int i = 0 ; i< acc.size() ; i++){
				names[k++] = c.getName();
			}
			}
		}
		k = 0;
	    for(Account aux : accounts){
		    data = new Vector<Object>();
		    data.addElement(names[k++]);
			for(int i = 0 ; i<=1;i++){
					if(i==0)
					data.addElement(aux.getId());
					else if(i==1)
					data.addElement(aux.getClass().getSimpleName());
				}
				mdl.addRow(data);
			}
		
		t.setModel(mdl);
		return t;
	}
	
}
