package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class Menu extends JFrame{
	private static final long serialVersionUID = 1L;
    JPanel contentPane;
	JButton client = new JButton("Client\r\n");
	JButton account = new JButton("Account\r\n");

	
	
	public Menu(){
		
		
		setTitle("Bank\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 200, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102,0,0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	
		client.setBounds(50,100,100,50);
		account.setBounds(50,220,100,50);
		contentPane.add(client);
		contentPane.add(account);
		client.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
			    ClientOperations co = new ClientOperations();
			    co.setVisible(true);
				dispose();
			}
		});
        account.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AccountOperations ao = new AccountOperations();
				ao.setVisible(true);
				dispose();
			}
		});
        
        setVisible(true);
	}
}
