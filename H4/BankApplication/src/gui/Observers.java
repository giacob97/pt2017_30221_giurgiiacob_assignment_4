package gui;

import java.util.ArrayList;
import java.util.Set;

import model.Account;
import model.Bank;
import model.Client;

public class Observers {

	public Observers() {
	}

	public void getObservers(Bank b){
		ArrayList<Client> clients = b.getClients();
		for(Client c : clients){
			Set<Account> acc = b.readAccounts(c);
			for(Account aux : acc){
				aux.addObserver(c);
				aux.addObserver(b);
			}
		}
	}
	
	
}
