package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import functions.InvariantMethod;

import java.util.Set;

public class Bank implements Observer,BankProc,Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<Client,Set<Account>> banca = new HashMap<Client,Set<Account>>();
	InvariantMethod iv;
	
	@Override
	public void addClient(Client c) {
		iv = new InvariantMethod();
		assert c.getName()!= null :"Numele este invalid";
		assert iv.checkBank(this);
		int pre = banca.size();
		banca.put(c,new HashSet<Account>(0));
	    int post = banca.size();
		assert post == pre + 1: "Nu s-a introdus corect";
		assert iv.checkBank(this);
	}

	@Override
	public void removeClient(Client c) {
		iv = new InvariantMethod();
		assert iv.checkBank(this);
		assert c.getName()!= null;
		int pre = banca.size();
		banca.remove(c);
	    int post = banca.size();
		assert post == pre - 1: "Nu s-a efectuat stergerea";
		assert iv.checkBank(this);
		assert iv.checkBank(this);
	}


	@Override
	public void addAccount(Client c, Account a) {
		iv = new InvariantMethod();
		assert a!= null :"Cont dorit este invalid";
		assert iv.checkBank(this);
		int pre = banca.get(c).size();
		a.addObserver(c);
		a.addObserver(this);
		banca.get(c).add(a);
	    int post = banca.get(c).size();
		assert post == pre + 1: "Nu s-a introdus corect";
		assert iv.checkBank(this);
		
	}

	@Override
	public void deleteAccount(Client c, int id) {
		iv = new InvariantMethod();
		int sters = 0;
		assert (id>0) :"Cont dorit este invalid";
		assert iv.checkBank(this);
		int pre = banca.get(c).size();
		Iterator<Account> it = banca.get(c).iterator();
		while(sters == 0){
			Account aux = it.next();
			if(aux.getId() == id){
				sters = 1;
				it.remove();
				System.out.println("S-a sters contul " + id);
			}
		}		
	    int post = banca.get(c).size();
		assert post == pre -1: "Nu s-a introdus corect";
		assert iv.checkBank(this);
		
	}
	
	@Override
	public void addMoney(Client c, Account a, double sum) {
		double b1, b;
		assert c!= null :"Utilizator invalid";
		assert (sum > 0);
		Set<Account> acc = readAccounts(c);
			for(Account aux : acc){
			     if(aux.getId() == a.getId()){
				     b = aux.getMoney();
				     aux.adaugare(sum);
				     b1 = aux.getMoney();
				     assert b1 == b + sum;
		}
	   }
		   
	}
	
	@Override
	public void withDrawMoney(Client c, int id, double sum) {
		
		int gasit = 0;
		assert c!= null :"Utilizator invalid";
		assert (sum > 0);
		Iterator<Account> it = banca.get(c).iterator();
		while(gasit == 0){
			Account aux = it.next();
			if(aux.getId() == id){
				gasit=1;
				aux.retragere(sum); /*
			    if(aux instanceof SpendingAccounts){
			    	   if(aux.getMoney() > sum){
			    		   aux.retragere(sum);
			    	   }
			    	   else {
			    		   System.out.println("Nu exista aceasta suma in cont");
			    	       assert (aux.getMoney() > sum);
			    	   }
			       }
			    else if(aux instanceof SavingAccounts){
			    	aux.retragere(sum);
			    }*/
			}
		}
		
	}
	
	@Override
	public Set<Account> readAccounts(Client c) {
		assert c!= null :"Client invalid";
		Set<Account> accounts = banca.get(c);
		return accounts;
	}
	
	@Override
	public ArrayList<Client> getClients() {
		ArrayList<Client> clients = new ArrayList<>();
		for(Entry<Client, Set<Account>> e : banca.entrySet()){
			clients.add(e.getKey());
		}
		return clients;
	}
	
	public Map<Client, Set<Account>> getBanca() {
		return banca;
	}

	public void setBanca(Map<Client, Set<Account>> banca) {
		this.banca = banca;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Banca a fost modificata");
		
	}
}
