package model;

import java.util.ArrayList;
import java.util.Set;

public interface BankProc {

	
	/**
	 * Adds a new Client in bank
	 * @pre c.getName()!= null;
	 * @post getSize() == getSize()@pre + 1; 
	 */
	public void addClient(Client c);
	
	/**
	 * Removes a new Client from the bank
	 * @pre c.getName()!=NULL;
	 * @post getSize() = getSize()@pre + 1; 
	 */
	public void removeClient(Client c);
	
	/**
	 * @pre  a!=null
	 * @post getSize() == getSize()@pre + 1; 
	 */
	public void addAccount(Client c,Account a);
	
	/**
	 * @pre  id<0
	 * @post getSize() == getSize()@pre - 1; 
	 */
	public void deleteAccount(Client c,int id);
	
	/**
	 * @pre c!=null
	 * @post @return 
	 */
	public Set<Account> readAccounts(Client c);
	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Client> getClients();
	
	/**
	 * 
	 * @pre sum > 0 && c!=null
	 * @post moneyAfter() = sum + moneyBefore();
	 */
	public void addMoney(Client c,Account a,double sum);
	
	
	/**
	 * 
	 * @pre sum > 0 && c!=null
	 * @post @return
	 */
	public void withDrawMoney(Client c,int id,double sum);
	
	
}
