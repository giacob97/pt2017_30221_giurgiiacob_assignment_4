package model;


import java.io.Serializable;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;
public class Client implements Observer,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
    private String name;
    private String obs;
     
    public Client(int id, String name) {
		this.id = id;
		this.name = name;
	}


	@Override
	public int hashCode() {
		int h = 7;
		if(name==null)
			h = h*31 + 0;
		else{
		h = h*31 + name.hashCode();
		}// am luat numar prim pentru a evita posibile coliziuni
		h= h*31+ id;
		return h;
	}
      
    
	 @Override
	 public boolean equals(Object o) {

	        if (o == this) return true;
	        if (!(o instanceof Client)) {
	            return false;
	        }
	        Client c = (Client) o;
	        return id==c.getId() && Objects.equals(name, c.name);
	 }       
	
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



	@Override
	public void update(Observable arg0, Object arg1) {
		   if(arg0 instanceof Account){
			   Account a = (Account) arg0;
		       setObs("S-a modificat depozitul din contul " + a.getId() +  " al utilizatorului " + getName() + " cu suma de " + arg1.toString()); 
	           System.out.println("Done");  
		   }
		    
	}


	public String getObs() {
		return obs;
	}


	public void setObs(String obs) {
		this.obs = obs;
	}
     
	
      
      
}
