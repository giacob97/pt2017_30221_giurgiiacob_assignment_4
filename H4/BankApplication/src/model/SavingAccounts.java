package model;

public class SavingAccounts extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private boolean executed = false;
	private double interest; 
    
	public SavingAccounts(int id,double money) {
		super(id,money);
		this.interest = 0;
	}


	public void retragere(double a) {
		interest = a - this.getMoney();
		interest = interest / this.getMoney() * 100;
		if(interest <= 10){
	    System.out.println("The sum " + a + "has been withdrawn");
		System.out.println("The interest is "+ interest + "%");
		this.setMoney(0);
		setChanged();
		notifyObservers(a);
		}
		else 
		{
			try {
				System.out.println("Invalid Withdraw because the maximum interest is 10%");
				System.out.println("Please try again!");
				assert interest < 10;
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}


	public void adaugare(double a) {
		if(!executed){
		  this.setMoney(this.getMoney() + a);
		  System.out.println("Notifying..");
		  setChanged();
		  notifyObservers(a);
		  this.executed = true;
		} 
	}  
}
