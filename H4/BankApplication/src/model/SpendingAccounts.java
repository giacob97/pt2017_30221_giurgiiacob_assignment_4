package model;

public class SpendingAccounts extends Account{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    
	
	public SpendingAccounts(int id, double money) {
		super(id, money);
	}

	public void adaugare(double a) {
		this.setMoney(this.getMoney() + a);
		System.out.println("Notifying..");
		setChanged();
		notifyObservers(a);
	}



	public void retragere(double a) {
		setMoney(this.getMoney() - a);
		System.out.println("Notifying..");
		setChanged();
		notifyObservers(a);
	}
	
	
}
